
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>NewCoFibre</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <!-- Third party plugin CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
          <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <style>
  .mySlides {display:none}
  </style>
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="https://www.newcofibre.co.za/portal/media/newcofibre.png" style="width:450px;"></a><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto my-2 my-lg-0">
                        <li class="nav-item"><a class="nav-link" href="index.html">HOME</a></li>
                        <li class="nav-item"><a class="nav-link" href="fibre.php">GET FIBRE</a></li>
                        <li class="nav-item"><a class="nav-link" href="contact.php">CONTACT</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="chead">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center text-center">
                    <div class="col-lg-10 align-self-end">
                        <h1 class="text-uppercase text-white font-weight-bold">Home Fibre</h1>
                        <br><br>
                    </div>
                </div>
            </div>
        </header>

        <!-- Call to action-->
        <section class="page-section bg-dark text-white">
        <div class="container">
                <div class="row">
                    <div class="align-self-end">
                    <h1 class="text-uppercase text-white font-weight-bold" style="text-align:center">Choose your package</h1>
                    </div>
                </div>
             <div class="row">
 
                
                
        
        
 
                
                
            
        <div class="col-lg-6 col-lg-offset-2  col-md-6 text-center">
                    <div class="mt-5">
                    
                    
                  
<div class="w3-content" style="max-width:800px">

      <div class='mySlides' style='width:100%; display:block'>
        <div class='col-lg-12 col-md-6 col-sm-12 col-xs-12' data-scroll-reveal='enter left move 30px over 0.6s after 0.4s'>
          <div class='features-item' style='border: 4px solid #03bfcb;border-radius: 20px;padding: 25px;background-color: #ffffff;color: #313131;text-align: center;'>
            <div class='features-icon'>
              <h1 style='margin-bottom:0.5em;font-size:60px;font-weight: 700;color: #004767;'>4Mbps</h1>
              <h2 style='margin-bottom:0.5em;font-weight: 700;color: #313131;'>R 500 /pm</h2><h3 style='color:#5f5f5f;font-size:20px;'>No Setup Fee</h3><br><a href='order-hardware.php?suid=35&puid=74' class='btn btn-lg btn-info pull-right'>Order Now</a></div>
          </div>
          <br>
        </div>
      </div><div class='mySlides' style='width:100%'>
        <div class='col-lg-12 col-md-6 col-sm-12 col-xs-12' data-scroll-reveal='enter left move 30px over 0.6s after 0.4s'>
          <div class='features-item' style='border: 4px solid #03bfcb;border-radius: 20px;padding: 25px;background-color: #ffffff;color: #313131;text-align: center;'>
            <div class='features-icon'>
              <h1 style='margin-bottom:0.5em;font-size:60px;font-weight: 700;color: #004767;'>10Mbps</h1>
              <h2 style='margin-bottom:0.5em;font-weight: 700;color: #313131;'>R 600 /pm</h2><h3 style='color:#5f5f5f;font-size:20px;'>No Setup Fee</h3><br><a href='order-hardware.php?suid=35&puid=71' class='btn btn-lg btn-info pull-right'>Order Now</a></div>
          </div>
          <br>
        </div>
      </div><div class='mySlides' style='width:100%'>
        <div class='col-lg-12 col-md-6 col-sm-12 col-xs-12' data-scroll-reveal='enter left move 30px over 0.6s after 0.4s'>
          <div class='features-item' style='border: 4px solid #03bfcb;border-radius: 20px;padding: 25px;background-color: #ffffff;color: #313131;text-align: center;'>
            <div class='features-icon'>
              <h1 style='margin-bottom:0.5em;font-size:60px;font-weight: 700;color: #004767;'>20Mbps</h1>
              <h2 style='margin-bottom:0.5em;font-weight: 700;color: #313131;'>R 650 /pm</h2><h3 style='color:#5f5f5f;font-size:20px;'>No Setup Fee</h3><br><a href='order-hardware.php?suid=35&puid=70' class='btn btn-lg btn-info pull-right'>Order Now</a></div>
          </div>
          <br>
        </div>
      </div><div class='mySlides' style='width:100%'>
        <div class='col-lg-12 col-md-6 col-sm-12 col-xs-12' data-scroll-reveal='enter left move 30px over 0.6s after 0.4s'>
          <div class='features-item' style='border: 4px solid #03bfcb;border-radius: 20px;padding: 25px;background-color: #ffffff;color: #313131;text-align: center;'>
            <div class='features-icon'>
              <h1 style='margin-bottom:0.5em;font-size:60px;font-weight: 700;color: #004767;'>50Mbps</h1>
              <h2 style='margin-bottom:0.5em;font-weight: 700;color: #313131;'>R 850 /pm</h2><h3 style='color:#5f5f5f;font-size:20px;'>No Setup Fee</h3><br><a href='order-hardware.php?suid=35&puid=72' class='btn btn-lg btn-info pull-right'>Order Now</a></div>
          </div>
          <br>
        </div>
      </div><div class='mySlides' style='width:100%'>
        <div class='col-lg-12 col-md-6 col-sm-12 col-xs-12' data-scroll-reveal='enter left move 30px over 0.6s after 0.4s'>
          <div class='features-item' style='border: 4px solid #03bfcb;border-radius: 20px;padding: 25px;background-color: #ffffff;color: #313131;text-align: center;'>
            <div class='features-icon'>
              <h1 style='margin-bottom:0.5em;font-size:60px;font-weight: 700;color: #004767;'>100Mbps</h1>
              <h2 style='margin-bottom:0.5em;font-weight: 700;color: #313131;'>R 1100 /pm</h2><h3 style='color:#5f5f5f;font-size:20px;'>No Setup Fee</h3><br><a href='order-hardware.php?suid=35&puid=73' class='btn btn-lg btn-info pull-right'>Order Now</a></div>
          </div>
          <br>
        </div>
      </div>
          </div>
    <div class='w3-center'>
      <button class='w3-button demo w3-cyan'>4Mbps</button>
      <button class='w3-button demo'>10Mbps</button>
      <button class='w3-button demo'>20Mbps</button>
      <button class='w3-button demo'>50Mbps</button>
      <button class='w3-button demo'>100Mbps</button>
    <div class='w3-section'>
           <button class='btn btn-info previous' data-update-value="-1">❮ Less</button>
           <button class='btn btn-info next' data-update-value="1">More ❯</button>
    </div>
    
    </div>
                    
                    
                  </div>  
                </div>   
                <div class="col-lg-1 text-center">  </div>  
                <div class="col-lg-5 col-md-6 text-center">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-center">
                    <div class="mt-5">
                    <i class="fas fa-4x fa fa-retweet text-primary mb-4"></i>                        
                    <h3 class="h4 mb-2">Uncapped Data</h3>
                        <p class="text-muted mb-0">Never have to watch your data limit ever again!</p>
                    </div>
                </div>
                 <div class="col-lg-6 col-md-6 text-center">
                    <div class="mt-5">
                        <i class="fas fa-4x fa fa-phone text-primary mb-4"></i>
                        <h3 class="h4 mb-2">Voice</h3>
                        <p class="text-muted mb-0">Get talking on our Voice network</p>
                    </div>
                </div>
                    
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 text-center">
                    <div class="mt-5">
                        <i class="fas fa-4x fa fa-wifi text-primary mb-4"></i>
                        <h3 class="h4 mb-2">FREE Router</h3>
                        <p class="text-muted mb-0">Give your house the WiFi it deserves.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 text-center">
                    <div class="mt-5">
                    <i class="fas fa-4x fa fa-thumbs-up text-primary mb-4"></i>                        
                    <h3 class="h4 mb-2">No Contract</h3>
                        <p class="text-muted mb-0">Our home services are all Month to Month</p>
                    </div>
                </div>
               
                    
            </div>
        </div>
                    
                </div>
            </div>
        </section>
        
        <!-- Footer-->
        <footer class="bg-light py-5">
            <div class="container"><div class="small text-center text-muted">Copyright © 2020 - NewCoFibre</div></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        
        <script>
          function updateSlide(index){
            var labels =  $(".w3-center .demo");
            labels.removeClass('w3-cyan');
            $(labels[index]).addClass('w3-cyan');
            var slides = $('.mySlides');
            $(slides).hide();
            $(slides[index]).css('display','block');
          }

          $(".w3-center .demo").click(function(e){
            e.preventDefault();
            updateSlide($(this).index());
          });

          $(".next, .previous").click(function(e){
            e.preventDefault();
            var currentSlide = $(".w3-center .demo.w3-cyan").index();
            var labelsLength = $(".w3-center .demo").length;
            var updateValue = Number($(this).attr('data-update-value'));
            var index = currentSlide + (updateValue);
            console.log(index);
            if(labelsLength === index){
              index = 0;
            }
            if(index == -1){
              index = labelsLength-1;
            }
            updateSlide(index);
          });
        </script>
    </body>
</html>
